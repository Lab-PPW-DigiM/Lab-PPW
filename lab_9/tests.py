from django.test import TestCase
from django.test import Client

import lab_9.csui_helper as CSUIHelper

import os
import environ

# Create your tests here.

root = environ.Path(__file__) - 2 # two folders back (/a/b/ - 2 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

valid_username = env("SSO_USERNAME")
valid_password = env("SSO_PASSWORD")

class Lab9UnitTest(TestCase):

    def test_lab_9_url_is_exist(self):
        response = Client().get('/lab-9/')
        self.assertEqual(response.status_code, 200)

    def test_lab_9_csui_helper(self):
        self.assertIsNotNone(CSUIHelper.get_access_token(valid_username, valid_password))
        self.assertIsNone(CSUIHelper.get_access_token("dummy_user", "dummy_password"))

        self.assertEquals(CSUIHelper.get_client_id(), "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")

        access_token = CSUIHelper.get_access_token(valid_username, valid_password)

        self.assertIn("username", CSUIHelper.verify_user(access_token))
        self.assertIn("error_description", CSUIHelper.verify_user("dummy_token"))

        self.assertIn("nama", CSUIHelper.get_data_user(access_token, "1606862702"))

