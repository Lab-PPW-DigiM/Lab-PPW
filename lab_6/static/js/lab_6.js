$(document).ready(function(){
    // kode jQuery selanjutnya akan ditulis disini

    $('.my-select').select2();

    var themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    	{"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    	{"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    	{"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    	{"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    	{"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    	{"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    	{"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    	{"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    	{"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    	{"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}];
    var selectedTheme = {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"};

    localStorage.setItem("themes", JSON.stringify(themes));
    localStorage.setItem("selectedTheme", JSON.stringify(selectedTheme));

    $('.my-select').select2({
    	'data' : themes
    });

    $('body').css('background', selectedTheme.bcgColor);
    $('body').css('color', selectedTheme.fontColor);

});

var isSending = true;

$('.chat-text textarea').keydown(function(e){
	if (e.keyCode == 13) {
		var msgClass;
		if(isSending){
			msgClass = 'msg-send';
		}else{
			msgClass = 'msg-receive';
		}
		var text = $('.chat-text textarea').val();

		var html = '<div class="' + msgClass + '">' + text + '</div>';
		$('.msg-insert').append(html);

		isSending = !isSending;
	}
});

$('.chat-text textarea').keyup(function(e){
	if(e.keyCode == 13) {
		$('.chat-text textarea').val("");
	}
});

// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = "";
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
  	if (erase) {
  		print.value = "";
  		erase = false;
  	}
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

$('.apply-button').on('click', function(){  // sesuaikan class button

    var themeId = $('.my-select').val();

    var themeObj = JSON.parse(localStorage.getItem('themes'))[themeId];
    console.log(themeObj);
    console.log(themeObj.bcgColor);
    console.log(themeObj.fontColor);

    $('body').css('background', themeObj.bcgColor);
    $('body').css('color', themeObj.fontColor);

    localStorage.setItem('selectedTheme', JSON.stringify(themeObj));
})
