from django.test import TestCase
from django.test import Client

from .models import Friend

from .api_csui_helper.csui_helper import CSUIhelper

# Create your tests here.

class Lab7UnitTest(TestCase):

    def test_lab_7_url_is_exist(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_lab_7_friend_list_url_is_exist(self):
    	response = Client().get('/lab-7/friend-list/')
    	self.assertEqual(response.status_code, 200)

    def test_lab_7_friend_list_json_is_exist(self):
    	friend = Friend.objects.create(friend_name="Dummy", npm="Dummy")
    	response = Client().get('/lab-7/get-friend-list/')
    	self.assertEqual(response.status_code, 200)

    def test_lab_7_add_friend_success(self):
    	response = Client().post('/lab-7/add-friend/', {'name': "Dummy", 'npm': "Dummy"})
    	self.assertEqual(response.status_code, 200)

    def test_lab_7_add_friend_fail(self):
    	friend = Friend.objects.create(friend_name="Dummy", npm="Dummy")
    	response = Client().post('/lab-7/add-friend/', {'name': "Dummy", 'npm': "Dummy"})
    	self.assertEqual(response.status_code, 400)

    def test_lab_7_delete_friend_success(self):
    	friend = Friend.objects.create(friend_name="Dummy", npm="Dummy")
    	response = Client().get('/lab-7/delete-friend/1/')
    	self.assertEqual(response.status_code, 302) # redirected to friend list

    def test_lab_7_validate_npm_is_not_taken(self):
    	response = Client().post('/lab-7/validate-npm/', {'npm': "12"})
    	self.assertEqual(response.status_code, 200)

    def test_lab_7_csui_helper(self):
    	csui_helper = CSUIhelper()

    	self.assertRaises(Exception, lambda: csui_helper.instance.get_access_token(True))
    	self.assertEquals(csui_helper.instance.get_client_id(), "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")
    	self.assertEqual(csui_helper.instance.get_auth_param_dict()["client_id"], "X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG")

