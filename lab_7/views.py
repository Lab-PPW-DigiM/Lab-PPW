from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers

from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
import os
import json

response = {}
#csui_helper = CSUIhelper(os.environ.get("SSO_USERNAME", "yourusername"),
#                         os.environ.get("SSO_PASSWORD", "yourpassword"))
csui_helper = CSUIhelper()

def index(request):
    # Page halaman menampilkan list mahasiswa yang ada
    # TODO berikan akses token dari backend dengan menggunakaan helper yang ada

    page = int(request.GET.get('page', '1'))

    data = csui_helper.instance.get_mahasiswa_list(page)

    friend_list = Friend.objects.all()
    response = {"data": data, "friend_list": friend_list, "next_page": page+1, "prev_page": page-1}
    html = 'lab_7/lab_7.html'
    return render(request, html, response)

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request):
    friend_list = []
    for friend in Friend.objects.all():
        friend_list.append(model_to_dict(friend))

    data = {
        'result' : friend_list
    }

    return JsonResponse(data)

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        name = request.POST['name']
        npm = request.POST['npm']

        if is_npm_taken(npm):
            return HttpResponse("Mahasiswa tersebut sudah ditambahkan sebagai teman", status=400)

        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        data = {
            'result' : model_to_dict(friend)
        }

        return JsonResponse(data)

def delete_friend(request, friend_id):
    Friend.objects.filter(id=friend_id).delete()
    return HttpResponseRedirect('/lab-7/friend-list/')

def is_npm_taken(npm):
    return len(Friend.objects.filter(npm=npm)) != 0

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': is_npm_taken(npm)
    }
    return JsonResponse(data)

def model_to_dict(obj):
    data = serializers.serialize('json', [obj,])
    struct = json.loads(data)

    tmp = struct[0]["fields"]
    tmp["id"] = struct[0]["pk"]

    #data = json.dumps(struct[0]["fields"])
    data = json.dumps(tmp)
    return data